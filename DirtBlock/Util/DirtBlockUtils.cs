﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace DirtBlock.Util {
    public static class DirtBlockUtils {
        public static Vector3 Snap(this Vector3 vector3, float gridSize = 1.0f)
        {
            return new Vector3(
                Mathf.Round(vector3.x / gridSize) * gridSize,
                Mathf.Round(vector3.y / gridSize) * gridSize,
                Mathf.Round(vector3.z / gridSize) * gridSize);
        }

        public static Vector3 SnapOffset(this Vector3 vector3, Vector3 offset, float gridSize = 1.0f)
        {
            Vector3 snapped = vector3 + offset;
            snapped = new Vector3(
                Mathf.Round(snapped.x / gridSize) * gridSize,
                Mathf.Round(snapped.y / gridSize) * gridSize,
                Mathf.Round(snapped.z / gridSize) * gridSize);
            return snapped - offset;
        }

        public static Vector3 SnapGOToParent(this GameObject child, GameObject parent, float gridSize = 1.0f)
        {
            BoxCollider collider = child.GetComponent<BoxCollider>();
            if(collider != null)
            {
                Vector3 offset = collider.size;
                return new Vector3(parent.transform.position.x + offset.x, parent.transform.position.y, parent.transform.position.z);
            }
            else
            {
                return Vector3.zero;
            }
            
        }

        public static Vector3 CalculateOffset(Vector3 parentPos, Vector3 ghostPos)
        {
            Vector3 diff = ghostPos - parentPos;
            Console.WriteLine("YOLO - diff vector: " + diff.ToString());
            float x = diff.x;
            float y = diff.y;
            float z = diff.z;

            string largest = "";

            if (Math.Abs(x) > Math.Abs(y)) largest = "x";
            else largest = "y";

            switch (largest)
            {
                case "x":
                    if (Math.Abs(x) < Math.Abs(z)) largest = "z";
                    break;
                case "y":
                    if (Math.Abs(y) < Math.Abs(z)) largest = "z";
                    break;
            }

            switch (largest)
            {
                case "x":
                    diff.y = 0.0f;
                    diff.z = 0.0f;
                    if (diff.x > 0f) diff.x = 1.0f;
                    else diff.x = -1.0f;
                    break;
                case "y":
                    diff.x = 0.0f;
                    diff.z = 0.0f;
                    if (diff.y > 0f) diff.y = 1.0f;
                    else diff.y = -1.0f;
                    break;
                case "z":
                    diff.x = 0.0f;
                    diff.y = 0.0f;
                    if (diff.z > 0f) diff.z = 1.0f;
                    else diff.z = -1.0f;
                    break;
            }
            
            // Something about the way construction targetting works makes this necessary
            if(y == 0.5f)
            {
                diff.y = -1f;
                diff.z = 0f;
                diff.x = 0f;
            }
            else if(y == -.5f){
                diff.y = 1f;
                diff.z = 0f;
                diff.x = 0f;
            }

            return diff;

        }
    }
}
