﻿using Harmony;
using System.Reflection;

namespace GentleReapers {
    public class MainPatcher {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.gentlereapers.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}
