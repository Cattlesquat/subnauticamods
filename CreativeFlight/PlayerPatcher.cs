﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using UnityEngine;

namespace CreativeFlight {

    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("Awake")]
    internal class Player_Awake_Patch {
        [HarmonyPostfix]
        public static void Postfix()
        {
            if(Player.main.IsInBase() || Player.main.IsInSubmarine() || Player.main.IsInSub() || Player.main.IsInside() || Player.main.IsInsideWalkable())
            {
                Console.WriteLine("YOLO - player is inside something or other");
                Player.main.rigidBody.useGravity = true;
                return;
            }
            else
            {
                Console.WriteLine("YOLO Freeeedommmmm!");
                Player.main.rigidBody.useGravity = false;
            }
        }
    }

    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("Update")]
    internal class Player_Update_Patch {
        private static float accel = 0.5f;
        [HarmonyPostfix]
        public static void Postfix()
        {
            if (Player.main.IsInBase() || Player.main.IsInSubmarine() || Player.main.IsInSub() || Player.main.IsInside() || Player.main.IsInsideWalkable() || Player.main.IsSwimming())
            {
                return;
            }
            if (!GameModeUtils.IsOptionActive(GameModeOption.Creative))
            {
                return;
            }
            Transform facing = Player.main.camRoot.GetAimingTransform();

            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                accel += 0.1f;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {

                if (accel < 0.1f)
                {
                    accel = 0.1f;
                }
            }

            if (Input.GetKey(KeyCode.LeftControl))
            {
                Vector3 pos = Player.main.transform.position;
                Player.main.transform.position = new Vector3(pos.x, pos.y - accel, pos.z);
            }
            if (Input.GetKey(KeyCode.Space))
            {
                Vector3 pos = Player.main.transform.position;
                Player.main.transform.position = new Vector3(pos.x, pos.y + accel, pos.z);
            }
            if (Input.GetKey(KeyCode.W))
            {
                Vector3 pos = Player.main.transform.position;
                Player.main.transform.position += facing.forward.normalized * accel;
            }
            if (Input.GetKey(KeyCode.A))
            {
                Vector3 pos = Player.main.transform.position;
                Player.main.transform.position += facing.right.normalized * accel * -1f;
            }
            if (Input.GetKey(KeyCode.S))
            {
                Vector3 pos = Player.main.transform.position;
                Player.main.transform.position += facing.forward.normalized * accel * -1f;
            }
            if (Input.GetKey(KeyCode.D))
            {
                Vector3 pos = Player.main.transform.position;
                Player.main.transform.position += facing.right.normalized * accel;
            }
        }
    }
}
