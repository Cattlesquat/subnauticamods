﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Harmony;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Options;
using UnityEngine;

namespace Unleashed_Lifepod {
    public class MainPatcher {

        public static bool podAnchored = false;

        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.unleashedlifepod.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            OptionsPanelHandler.RegisterModOptions(new Options("Unleashed Lifepod"));
            Options.InitializeConfig();
        }

    }

    public class Options : ModOptions {

        public Options(string name) : base(name)
        {

            try
            {
                ToggleChanged += OnToggleChanged;
                ChoiceChanged += OnChoiceChanged;
                KeybindChanged += OnKeyBindChanged;

            }
            catch (Exception)
            {

            }
        }

        public override void BuildModOptions()
        {
            try
            {
                List<String> spawnOptions = new List<string>();
                spawnOptions.Add("Random");
                spawnOptions.Add("Safe Shallows");
                spawnOptions.Add("Landfall");
                spawnOptions.Add("Mountain Island");
                spawnOptions.Add("Floating Island");
                spawnOptions.Add("Oasis");
                spawnOptions.Add("Dunes");
                spawnOptions.Add("Custom");

                AddToggleOption("heavyPod", "Heavy Pod", UnleashedLifepodConfig.heavyPod);
                AddToggleOption("allowControl", "Allow controllable pod", UnleashedLifepodConfig.allowControl);
                AddKeybindOption("unleashedlifepod_modifier_key", "Drive Mode Key", GameInput.Device.Keyboard, UnleashedLifepodConfig.keyBinds["unleashedlifepod_modifier_key"]);
                AddKeybindOption("unleashedlifepod_anchor_key", "Anchor Pod Key", GameInput.Device.Keyboard, UnleashedLifepodConfig.keyBinds["unleashedlifepod_anchor_key"]);
                AddChoiceOption("podSpawn", "Spawn Location", spawnOptions.ToArray(), 0);
            }
            catch (Exception)
            {
                
            }
        }

        public void OnKeyBindChanged(object sender, KeybindChangedEventArgs e)
        {
            switch (e.Id)
            {
                case "unleashedlifepod_modifier_key":
                    UnleashedLifepodConfig.keyBinds["unleashedlifepod_modifier_key"] = e.Key;
                    UpdatePlayerPrefs();
                    break;
                case "unleashedlifepod_anchor_key":
                    UnleashedLifepodConfig.keyBinds["unleashedlifepod_anchor_key"] = e.Key;
                    UpdatePlayerPrefs();
                    break;
            }
            
        }

        public void OnChoiceChanged(object sender, ChoiceChangedEventArgs e)
        {
            UnleashedLifepodConfig.spawnSelection = e.Value;
        }

        public void OnToggleChanged(object sender, ToggleChangedEventArgs e)
        {
            try
            {
                string id = e.Id;
                switch (id)
                {
                    case "heavyPod":
                        UnleashedLifepodConfig.heavyPod = e.Value;
                        break;
                    case "allowControl":
                        UnleashedLifepodConfig.allowControl = e.Value;
                        break;
                    default:
                        Console.WriteLine("ERROR: UnleashedLifepod invalid onToggleChanged id");
                        break;
                }
                UpdatePlayerPrefs();

            }
            catch (Exception)
            {
                Console.WriteLine("ERROR: UnleashedLifepod onToggleChanged for " + e.Id);
            }
        }


        // Update PlayerPrefs when options of interest are changed.
        void UpdatePlayerPrefs()
        {
            foreach (string key in UnleashedLifepodConfig.keyBinds.Keys)
            {
                int intKey = (int)UnleashedLifepodConfig.keyBinds[key];
                PlayerPrefs.SetInt(key, intKey);
            }

            if (UnleashedLifepodConfig.heavyPod)
            {
                PlayerPrefs.SetInt("heavyPod", 1);
            }
            else
            {
                PlayerPrefs.SetInt("heavyPod", 0);
            }

            if (UnleashedLifepodConfig.allowControl)
            {
                PlayerPrefs.SetInt("allowControl", 1);
            }
            else
            {
                PlayerPrefs.SetInt("allowControl", 0);
            }

            PlayerPrefs.Save();
        }

        // Load previously selected mod options from PlayerPrefs
        public static void InitializeConfig()
        {
            if (PlayerPrefs.HasKey("unleashedlifepod_modifier_key"))
            {
                UnleashedLifepodConfig.keyBinds["unleashedlifepod_modifier_key"] = (KeyCode)PlayerPrefs.GetInt("unleashedlifepod_modifier_key");
            }
            else
            {
                if(UnleashedLifepodConfig.keyBinds.ContainsKey("unleashedlifepod_modifier_key")){

                    UnleashedLifepodConfig.keyBinds["unleashedlifepod_modifier_key"] = KeyCode.LeftShift;
                }
                else
                {
                    UnleashedLifepodConfig.keyBinds.Add("unleashedlifepod_modifier_key", KeyCode.LeftShift);
                }

            }

            if (PlayerPrefs.HasKey("unleashedlifepod_anchor_key"))
            {
                UnleashedLifepodConfig.keyBinds["unleashedlifepod_anchor_key"] = (KeyCode)PlayerPrefs.GetInt("unleashedlifepod_anchor_key");
            }
            else
            {
                UnleashedLifepodConfig.keyBinds.Add("unleashedlifepod_anchor_key", KeyCode.LeftControl);
            }

            if (PlayerPrefs.HasKey("heavyPod"))
            {
                if (PlayerPrefs.GetInt("heavyPod") == 0)
                {
                    UnleashedLifepodConfig.heavyPod = false;
                }
                else
                {
                    UnleashedLifepodConfig.heavyPod = true;
                }
            }

            if (PlayerPrefs.HasKey("allowControl"))
            {
                if (PlayerPrefs.GetInt("allowControl") == 0)
                {
                    UnleashedLifepodConfig.allowControl = false;
                }
                else
                {
                    UnleashedLifepodConfig.allowControl = true;
                }
            }
        }

    }

    public class UnleashedLifepodConfig : MonoBehaviour {
        public static string spawnSelection = "Safe Shallows";
        public static bool heavyPod = false;
        public static bool allowControl = false;
        public static Dictionary<string, KeyCode> keyBinds = new Dictionary<string, KeyCode>();
        
       
    }
}
